

import networkx as nx
import math
#import datasets  


def createPhi(H,G,filename):
    
    H = normalizeNodes(H)
    G = normalizeNodes(G)
        
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    eG = len(G.edges())
    
    nVars = n*m
    nCl =  int(m + int(m*n*(n-1)/2) + eH * (n*n-2*eG))
    
    def x(v,w):    return str(v*n + w + 1) #variable index in cnf (>0) as string
    def neg(s): return '-' + s #negation
    
    with open(filename, "w") as f: # overwrite the file if it already exists
        f.write('p cnf {} {}\n'.format(nVars,nCl) )
        
        # phi_1
        for v in H.nodes:
            clause = ''
            for w in G.nodes:
                clause += x(v,w) +' '
            f.write(clause+'0\n')
        
        #phi_2
        for v in H.nodes:
            for j in range(n-1):
                for j2 in range(j+1,n):
                    f.write(neg(x(v,j)) +' '+ neg(x(v,j2)) +' 0\n')
        
        #phi_3
        for w in G.nodes:
            for w2 in G.nodes:
                if( (w,w2) not in G.edges and (w2,w) not in G.edges):
                    for (v,v2) in H.edges:
                        f.write(neg(x(v,w)) +' '+ neg(x(v2,w2)) +' 0\n')    
        
    #end of with (closes file)    

def createPhi1_log(H,G,filename):
    
    H = normalizeNodes(H)
    G = normalizeNodes(G)
        
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    eG = len(G.edges())
    
    l = math.ceil(math.log2(n))
    nz= sum( [(n-1)&(1<<i)==0 for i in range(l)] ) #number of zero bits in maximal node
    
    nVars = m*l
    nCl =   eH * (n*n-2*eG) + m*nz
    
    def x(v,i):     return str(v*l + i + 1)
    def lit(v,w,i): # literal in phi_3
        if  w & 1<<i > 0:
            return '-' + x(v,i) 
        else:
            return x(v,i)
    
    with open(filename, "w") as f: # overwrite the file if it already exists
        f.write('p cnf {} {}\n'.format(nVars,nCl) )
        
        #phi_3
        for w in G.nodes:
            for w2 in G.nodes:
                if( (w,w2) not in G.edges and (w2,w) not in G.edges):
                    for (v,v2) in H.edges:
                        clause = ''
                        for i in range(l):
                            clause +=  lit(v,w,i) +' '+ lit(v2,w2,i) +' '
                        f.write(clause +'0\n') 
                       
        #valid range (sum_(x_vi * 2^i)<n
        ones = [];
        for i in range(l-1,-1,-1):
            if (n-1) & 1<<i == 0: #zero in binary representation
                for v in H.nodes:
                    clause = '-'+ x(v,i) +' '
                    for o in ones:
                        clause += '-'+ x(v,o) +' '
                    f.write(clause +'0\n')   
            else:
                ones += [i]
                
def createPhi1_log_filled(H,G,filename):
    
    H = normalizeNodes(H)
    G = normalizeNodes(G)
        
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    eG = len(G.edges())
    
    l = math.ceil(math.log2(n))
    
    for i in range(n,2**l): #add isolated nodes to
        G.add_node(i)        
    n = len(G.nodes())
    
    nz = sum( [(n-1)&(1<<i)==0 for i in range(l)] ) #number of zero bits in maximal node
    mi = sum([1 for v in H.nodes  if H.degree(v)==0]) #number of isolated nodes in H
    
    nVars = m*l
    nCl =   eH * (n*n-2*eG) + mi*nz
    
    def x(v,i):     return str(v*l + i + 1)
    def lit(v,w,i): # literal in phi_3
        if  w & 1<<i > 0:
            return '-' + x(v,i)
        else:
            return x(v,i)
    
    with open(filename, "w") as f: # overwrite the file if it already exists
        f.write('p cnf {} {}\n'.format(nVars,nCl) )
        
        #phi_3
        for w in G.nodes:
            for w2 in G.nodes:
                if( (w,w2) not in G.edges and (w2,w) not in G.edges):
                    for (v,v2) in H.edges:
                        clause = ''
                        for i in range(l):
                            clause +=  lit(v,w,i) +' '+ lit(v2,w2,i) +' '
                        f.write(clause +'0\n')
        
        #valid range (sum_(x_vi * 2^i)<n
        ones = [];
        for i in range(l-1,-1,-1):
            if (n-1) & 1<<i == 0: #zero in binary representation
                for v in H.nodes:
                    if H.degree(v)==0:
                        clause = '-'+ x(v,i) +' '
                        for o in ones:
                            clause += '-'+ x(v,o) +' '
                        f.write(clause +'0\n')
            else:
                ones += [i]
            
        
    #end of with (closes file)    
    
def createPhi_Eclauses(H,G,filename):
        
    H = normalizeNodes(H)
    G = normalizeNodes(G)
    
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    eG = len(G.edges())
    
    nVars = n*m
    nCl =  int(m + eH * (n*n-2*eG))
    
    def x(v,w):    return str(v*n + w + 1) #variable index in cnf (>0) as string
    def neg(s): return '-' + s #negation
    
    with open(filename, "w") as f: # overwrite the file if it already exists
        f.write('p cnf {} {}\n'.format(nVars,nCl) )
        f.write('eclauses {}\n'.format(m) )
        
        #phi_3
        for w in G.nodes:
            for w2 in G.nodes:
                if( (w,w2) not in G.edges and (w2,w) not in G.edges):
                    for (v,v2) in H.edges:
                        f.write(neg(x(v,w)) +' '+ neg(x(v,w2)) +' 0\n')    
        
        # phi_1
        for v in H.nodes:
            clause = ''
            for w in G.nodes:
                clause += x(v,w) +' ' #'{} '.format(i*n+j+1)
            f.write(clause+'0\n')
                
    #end of with (closes file)

    
def createPhi2_Tseytin(H,G,filename):
    
    H = normalizeNodes(H)
    G = normalizeNodes(G)
    
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    eG = len(G.edges())
    
    H_components = nx.connected_components(H)
    nH_components = 0
    h_roots = []
    for comp in H_components:
        h_roots += [comp.pop()] #arbitrary node from component
        nH_components += 1
    
    nVars = 3*n*m
    nCl =  nH_components + n*m*(n+2) + n*eH + 2*m*eG
    
    for h_root in h_roots:
        H = directedGraph(H,h_root)
    
    def x(t,v):    return str(        t*n + v + 1) #variable index in cnf (>0) as string
    def y(t,v): return str(  n*m + t*n + v + 1)
    def z(t,v): return str(2*n*m + t*n + v + 1)
    def neg(s): return '-' + s #negation
    
    with open(filename, "w") as f: # overwrite the file if it already exists
        f.write('p cnf {} {}\n'.format(nVars,nCl) )
        
        # root clauses
        for h_root in h_roots:
            clause = ''
            for w in range(n):
                clause += y(h_root,w) + ' ' 
            f.write(clause + '0\n')
        
        # alpha & beta formula
        for t in H.nodes:
            for v in G.nodes:
                #alpha clauses
                clause = y(t,v) #long clause (init)
                for t2 in H.successors(t):
                    f.write(neg(y(t,v)) +' '+ z(t2,v) +' 0\n' ) # binary clause
                    clause += ' '+ neg(z(t2,v))
                f.write( neg(y(t,v)) +' '+ x(t,v) +' 0\n' ) # binary clause
                clause += ' '+ neg(x(t,v))
                for w in G.nodes:
                    if not v==w:
                        f.write(neg(y(t,v)) +' '+ neg(x(t,w)) +' 0\n' ) # binary clause
                        clause += ' '+ x(t,w)
                f.write(clause + ' 0\n' ) # long clause (write)
                
                #beta clauses
                clause = neg(z(t,v)) #long clause (init)
                for w in G.neighbors(v):
                    f.write(z(t,v) +' '+ neg(y(t,w)) +' 0\n' ) # binary clause
                    clause += ' '+ y(t,w)
                f.write(clause + ' 0\n' ) # long clause (write)
            
        
    #end of with (closes file)

def createPhi3(H,G,filename):
    
    H = normalizeNodes(H)
    G = normalizeNodes(G)
    
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    eG = len(G.edges())
    
    H_components = nx.connected_components(H)
    nH_components = 0
    h_roots = []
    for comp in H_components:
        h_roots += [comp.pop()] #arbitrary node from component
        nH_components += 1
    
    nVars = n*m
    nCl =  nH_components + int(m*n*(n-1)/2) + n*eH
    
    for h_root in h_roots:
        H = directedGraph(H,h_root)
    
    def x(t,v):    return str(        t*n + v + 1) #variable index in cnf (>0) as string
    def neg(s): return '-' + s #negation
    
    with open(filename, "w") as f: # overwrite the file if it already exists
        f.write('p cnf {} {}\n'.format(nVars,nCl) )
        
        # root clauses
        for h_root in h_roots:
            clause = ''
            for w in range(n):
                clause += x(h_root,w) + ' ' 
            f.write(clause + '0\n')
        
        # C_{t,t',v}
        for t in H.nodes:
            for v in G.nodes:
                for t2 in H.successors(t):
                    clause = neg(x(t,v)) +' '
                    for w in G.neighbors(v):
                        clause += x(t2,w) +' '
                    f.write(clause +'0\n') 
        
        # phi_2 / C_{t,v,w}
        for v in H.nodes:
            for j in range(n-1):
                for j2 in range(j+1,n):
                    f.write(neg(x(v,j)) +' '+ neg(x(v,j2)) +' 0\n')
                    

def createPhi3_log_Tseytin(H,G,filename):
    
    H = normalizeNodes(H)
    G = normalizeNodes(G)
    
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    
    l = math.ceil(math.log2(n))
    
    nz = sum( [(n-1)&(1<<i)==0 for i in range(l)] ) #number of zero bits in maximal node
    
    H_components = nx.connected_components(H)
    nH_components = 0
    h_roots = []
    for comp in H_components:
        h_roots += [comp.pop()] #arbitrary node from component
        nH_components += 1
    
    nVars = m*(l+n)
    nCl =  nH_components + n*eH + m*n*l
    
    for h_root in h_roots:
        H = directedGraph(H,h_root)
    
    
    def x(t,i):     return str(t*l + i + 1)
    def lit(t,v,i): # positive literal
        if  v & 1<<i > 0:
            return x(t,i)
        else:
            return '-' + x(t,i)
    def nlit(t,v,i): # negated literal
        if  v & 1<<i > 0:
            return '-' + x(t,i)
        else:
            return x(t,i)
    def y(t,v):     return str(m*l + t*n + v +1)
    
    with open(filename, "w") as f: # overwrite the file if it already exists
        f.write('p cnf {} {}\n'.format(nVars,nCl) )
        
        # root clauses
        for h_root in h_roots:
            clause = ''
            for w in range(n):
                clause += y(h_root,w) + ' ' 
            f.write(clause + '0\n')
        
        # C_{t,t',v}
        for t in H.nodes:
            for v in G.nodes:
                for t2 in H.successors(t):
                    clause = '-'+ y(t,v) +' '
                    for w in G.neighbors(v):
                        clause += y(t2,w) +' '
                    f.write(clause +'0\n') 
                 
        # y(t,v) -> lit(t,v,i)
        for t in H.nodes:
            for v in G.nodes:
                for i in range(l):
                    f.write( '-'+ y(t,v) +' ' + lit(t,v,i) +' 0\n'.format() )
        
        
                           
                    


def createPhi3_log(H,G,filename):
    
    H = normalizeNodes(H)
    G = normalizeNodes(G)
    
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    
    l = math.ceil(math.log2(n))
    
    nz = sum( [(n-1)&(1<<i)==0 for i in range(l)] ) #number of zero bits in maximal node
    
    H_components = nx.connected_components(H)
    nH_components = 0
    h_roots = []
    for comp in H_components:
        h_roots += [comp.pop()] #arbitrary node from component
        nH_components += 1
    
    nVars = m*l
    nCl =  eH*sum([l**G.degree(v)  for v in G.nodes]) + m*nz
    
    for h_root in h_roots:
        H = directedGraph(H,h_root)
    
    
    def x(v,i):     return str(v*l + i + 1)
    def lit(v,w,i): # positive literal
        if  w & 1<<i > 0:
            return x(v,i)
        else:
            return '-' + x(v,i)
    def nlit(v,w,i): # negated literal
        if  w & 1<<i > 0:
            return '-' + x(v,i)
        else:
            return x(v,i)
    
    with open(filename, "w") as f: # overwrite the file if it already exists
        f.write('p cnf {} {}\n'.format(nVars,nCl) )
        
        # C_{t,t',v}
        for v in G.nodes:
            N = list(G.neighbors(v))
            d = len(N)
            I = [0]*d
            while I[d-1]<l:
                for t in H.nodes:
                    for t2 in H.successors(t):
                        clause = ''
                        for i in range(l):
                            clause += nlit(t,v,i) +' '
                        for i in range(d):
                            clause += lit(t2,N[i],I[i]) +' '
                        f.write(clause +'0\n') 
                I[0] += 1
                i=0
                while I[i]>=l and  i<d-1:
                    I[i] = 0
                    I[i+1] += 1
                    i += 1
        
        #valid range (sum_(x_vi * 2^i)<n
        ones = [];
        for i in range(l-1,-1,-1):
            if (n-1) & 1<<i == 0: #zero in binary representation
                for v in H.nodes:
                    clause = '-'+ x(v,i) +' '
                    for o in ones:
                        clause += '-'+ x(v,o) +' '
                    f.write(clause +'0\n')   
            else:
                ones += [i]
        
    #end of with (closes file)
        

def directedGraph(G, root):
#converts the graph into directed acyclical graph rooted in root
    G = nx.DiGraph(G)
    curN = (root,) #tuple
    while len(curN)>0 :
        nextN = ()
        for v in curN:
            for w in G.successors(v):
                if not (w,v) in G.edges:
                    break #node was already visited
                G.remove_edge(w,v)
                #print('removed: '+ str(w) +' | '+ str(v))
                if not w in nextN: #add successors to next step
                    nextN += (w,)
        curN = nextN
        
    return G
    
def normalizeNodes(G):
#relables the nodes with numbers starting with 0
    mapping = {};
    index = 0;
    for v in G.nodes:
        mapping[v] = index;
        index += 1
    return nx.relabel_nodes(G,mapping)
    

    
    


'''
if __name__ == '__main__':
    p = datasets.Parser("TestGraphs", ".")
    graphs = p.read_graph()
    filename = "Output/TestGraphs_{}.cnf"
    
    H = next(graphs)
    G = next(graphs)
    
    createPhi(H,G, filename.format(1))

    for i in range(3): #graph in graphs:
        graph = next(graphs)
        createPhi(graph,graph, filename.format(i))
        #i += 1
    '''
    
