import networkx as nx
from networkx.algorithms.approximation import treewidth_min_degree
import math  


def createPhi_dyn(H,G,filename): #log
    
    H = normalizeNodes(H)
    G = normalizeNodes(G)
    
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    eG = len(G.edges())
    
    H_components = nx.connected_components(H)
    nH_components = 0
    h_roots = []
    for comp in H_components:
        h_roots += [comp.pop()] #arbitrary node from component
        nH_components += 1
    
    for h_root in h_roots:
        H = directedGraph(H,h_root)
        
    l = math.ceil(math.log2(n))
    
    class OrTree():
        def __init__(self,vList,i=0,v=0):
            if len(vList)>1:
                left = [v   for v in vList   if (v&1<<i)==0]
                while len(left)==0 or  len(left)==len(vList):
                    i += 1
                    left = [v   for v in vList   if (v&1<<i)==0]
                right = [v   for v in vList  if not (v&1<<i)==0]
                self.left = OrTree(left, i+1,v)
                self.right = OrTree(right, i+1,v+2**i)
                self.i = i
                self.value = -1
            else:
                self.value = vList[0]

        def write(self, f,varI,sf,cur):
            if self.value == -1:
                i1,cur = self.left.write(f,varI,sf,cur)
                i2,cur = self.right.write(f,varI,sf,cur)
                f.write( 'O {j:d} 2 {i1:d} {i2:d}\n'.format(j=(varI+self.i+1), i1=i1, i2=i2) )
                return cur, cur+1
            else:
                return sf[self.value], cur
        def string(self):
            if self.value == -1:
                s1 = self.left.string()
                s2 = self.right.string()
                return '[ {i:d}: {s1:s} | {s2:s} ]'.format(i=self.i, s1=s1,s2=s2)
            else:
                return str(self.value)
    #connected nodes (as opposed to isolated)
    mCon = sum( [1   for t in H.nodes  if H.out_degree(t)>0] )
    nCon = sum( [1   for v in G.nodes  if G.degree(v)>0] )
    
    nNodes = 1+ nH_components*(n-1) + eH*(2*eG-nCon) + m*n + 2*m*l+1 #roots, neighbors, subformulas , literals
    nEdges = nH_components*(2*n-1)+ eH*(4*eG-2*nCon)+mCon*(n-nCon) + m*n*l #roots, neighbors,false, X(t,v)
    nVars = m*l
    
    with open(filename, "w") as f:
        f.write('nnf {} {} {}\n'.format(nNodes,nEdges, nVars) )
        
        #leafs 
        def x(t,i): return str( 2*(t*l + i) ) #indices of the literal nodes
        def notx(t,i): return str( 2*(t*l + i)+1 )
        falseNode = str(2*l*m)
        
        for t in range(m):
            for i in range(l):
                f.write( 'L {var:d}\nL -{var:d}\n'.format(var = (t*l+i+1) ))
        f.write( 'O 0 0\n' )
        
        #subformulas
        sf = [[0]*n    for i in range(m)] #indices of the subformula nodes (m*n matrix)
        cur = 2*l*m+1 #cur nnf-nodes id
        
        for t in reversed(list(nx.topological_sort(H))):
            for v in range(n):
                line = 'A '+ str( l + H.out_degree(t) )
                Nv = list(G.neighbors(v))
                if len(Nv)>1:
                    orTreeN = OrTree( Nv )
                    for t2 in H.successors(t):
                        i,cur = orTreeN.write(f,t2*l,sf[t2],cur)
                        line += ' '+ str(i)
                elif len(Nv)==1:
                    for t2 in H.successors(t):
                        line += ' '+ str( sf[t2][Nv[0]] )
                #else len(Nv)==0:
                elif H.out_degree(t)>0:
                    line = 'A '+ str(l+1) +' '+ falseNode
                else:
                    line = 'A '+ str(l)
                for i in range(l): #X(t,v)
                    if (v & 1<<i)>0: #one in binary
                        line += ' '+ x(t,i)
                    else:
                        line += ' '+ notx(t,i)
                f.write( line +'\n' )
                sf[t][v] = cur
                cur += 1
        
        #root
        line = 'A '+ str( nH_components ) 
        orTreeG = OrTree( range(n) )
        for h_root in h_roots:
            i,cur = orTreeG.write(f,h_root*l,sf[h_root],cur)
            line += ' '+ str(i)
        f.write( line +'\n' )
        
        assert cur+1==nNodes, "Incorrect number of nodes"
        
        
def createPhi_dyn_full(H,G,filename): #not log
    
    H = normalizeNodes(H)
    G = normalizeNodes(G)
    
    m = len(H.nodes())
    n = len(G.nodes())
    eH = len(H.edges())
    eG = len(G.edges())
    
    H_components = nx.connected_components(H)
    nH_components = 0
    h_roots = []
    for comp in H_components:
        h_roots += [comp.pop()] #arbitrary node from component
        nH_components += 1
    
    for h_root in h_roots:
        H = directedGraph(H,h_root)
        
    
    #connected nodes (as opposed to isolated)
    mCon = sum( [1   for t in H.nodes  if H.out_degree(t)>0] )
    nCon = sum( [1   for v in G.nodes  if G.degree(v)>0] )
    selfLoop = sum( [1  for v in G.nodes   if v in G.neighbors(v)] )
    
    nNodes = 1+ nH_components*(n-1) + eH*(2*eG-nCon-selfLoop) + m*n + 2*m*n+1 #roots, neighbors, subformulas , literals
    nEdges = nH_components*(2*n-1)+ eH*(4*eG-2*nCon-2*selfLoop)+mCon*(n-nCon) + m*n*n #roots, neighbors,false, X(t,v)
    nVars = m*n
    
    with open(filename, "w") as f:
        f.write('nnf {} {} {}\n'.format(nNodes,nEdges, nVars) )
        
        #leafs 
        def x(t,v): return str( 2*(t*n + v) ) #indices of the literal nodes
        def notx(t,v): return str( 2*(t*n + v)+1 )
        falseNode = str(2*m*n)
        
        for t in range(m):
            for v in range(n):
                f.write( 'L {var:d}\nL -{var:d}\n'.format(var = (t*n+v+1) ))
        f.write( 'O 0 0\n' )
        
        #subformulas
        sf = [[0]*n    for i in range(m)] #indices of the subformula nodes (m*n matrix)
        cur = 2*m*n+1 #cur nnf-nodes id
        
        for t in reversed(list(nx.topological_sort(H))):
            for v in range(n):
                line = 'A '+ str( n + H.out_degree(t) )
                Nv = list(G.neighbors(v))
                if len(Nv)>0:
                    for t2 in H.successors(t):
                        last = sf[t2][Nv[0]]
                        for w in Nv[1:]:
                            f.write( 'O {j:d} 2 {i1:d} {i2:d}\n'.format(j=t2*n+w+1, i1=last, i2=sf[t2][w]) )
                            last = cur
                            cur += 1
                        line += ' '+ str(last) 
                #elif len(Nv)==1:
                #    for t2 in H.successors(t):
                #        line += ' '+ str( sf[t2][Nv[0]] )
                #else len(Nv)==0:
                elif H.out_degree(t)>0:
                    line = 'A '+ str(n+1) +' '+ falseNode
                else:
                    line = 'A '+ str(n)
                    
                for w in range(n): #X(t,v)
                    if v==w:
                        line += ' '+ x(t,w)
                    else:
                        line += ' '+ notx(t,w)
                f.write( line +'\n' )
                sf[t][v] = cur
                cur += 1
        
        #root
        line = 'A '+ str( nH_components ) 
        for h_root in h_roots:
            last = sf[h_root][0]
            for v in range(1,n):
                f.write( 'O {j:d} 2 {i1:d} {i2:d}\n'.format(j=h_root*n+v+1, i1=last, i2=sf[h_root][v]) )
                last = cur
                cur += 1
            line += ' '+ str(last)
        f.write( line +'\n' )
        
        assert cur+1==nNodes, "Incorrect number of nodes ({:d} insted of {:d})".format(cur+1,nNodes)

'''
def createPhi_T(H,G,filename):
    H = normalizeNodes(H)
    G = normalizeNodes(G)
    
    m = len(H.nodes())
    n = len(G.nodes())
    
    treewidth, decomposition = treewidth_min_degree(H)
    decomposition = nx.relabel.convert_node_labels_to_integers(
            decomposition, label_attribute="bag" )
    
    
    nNodes = m*l+2 + 0
    nEdges = 0
    nVars = m*l
    
    with open(filename, "w") as f:
        f.write('nnf {} {} {}\n'.format(nNodes,nEdges, nVars) )
        
        #leafs 
        def x(t,i): return str( 2*(t*l + i) ) #indices of the literal nodes
        def notx(t,i): return str( 2*(t*l + i)+1 )
        falseNode = str(2*l*m)
        trueNode = str(2*l*m+1)
        
        for t in range(m):
            for i in range(l):
                f.write( 'L {var:d}\nL -{var:d}\n'.format(var = (t*l+i+1) ))
        f.write( 'O 0 0\n' )
        f.write( 'A 0\n' )
'''    

def directedGraph(G, root):
#converts the graph into directed acyclical graph rooted in root
    G = nx.DiGraph(G)
    curN = (root,) #tuple
    while len(curN)>0 :
        nextN = ()
        for v in curN:
            for w in G.successors(v):
                if not (w,v) in G.edges:
                    break #node was already visited
                G.remove_edge(w,v)
                #print('removed: '+ str(w) +' | '+ str(v))
                if not w in nextN: #add successors to next step
                    nextN += (w,)
        curN = nextN
        
    return G
    
def normalizeNodes(G):
#relables the nodes with numbers starting with 0
    mapping = {};
    index = 0;
    for v in G.nodes:
        mapping[v] = index;
        index += 1
    return nx.relabel_nodes(G,mapping)
    

