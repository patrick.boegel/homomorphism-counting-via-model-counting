import networkx as nx
from GraphParser.datasets import Parser

class caterpillar_Parser: # triangular caterpillar
    def __init__(self,n):
        self.n = int(n)

    def read_graph(self):    
        G = nx.Graph()
        i = 1
        cur = 1
        last = 0
        while i<=self.n:
            G.add_edge(last,cur)
            for j in range(1,i):
                G.add_edge(cur,cur+j)
            last = cur
            cur += i+1
            i += 1
            yield G
        return
        
class graph_AIDS11_Parser():
    def read_graph(self):    
        graphs = Parser('AIDS', 'GraphParser').read_graph()
        for i in range(11):
            G = next(graphs)
        yield G
        return
        
