#!/usr/bin/env python3

import os
import networkx as nx
import numpy as np


class Parser:
    """"Parser of the Dortmund graph format
        Usage:  1)
                Initialize a parser object with name of the data set and the path where it can be found.
                Use the functions read_graph() and read_label() to get the graph objects and the corresponding labels
                one-by-one.
                The will be computed via the generator (yield statements) just as you call it.
                2)
                If you just want to have the total list of graphs you can use the data function.

                See examples at end of the file.
    """

    def __init__(self, data_set, path):
        self.ds_a = os.path.join(path, data_set, data_set + '_A' + '.txt')
        self.ds_gi = os.path.join(path, data_set, data_set + '_graph_indicator' + '.txt')
        self.ds_gl = os.path.join(path, data_set, data_set + '_graph_labels' + '.txt')
        #self.ds_nl = os.path.join(path, data_set, data_set + '_node_labels' + '.txt')
        # self.ds_el = os.path.join(path, data_set, data_set+'_edge_labels'+'.txt')
        # TODO: Other optional Fileformats

    def _read_vertices(self):
        """Read the vertices from the graph indicator file and returns them after a graph is complete."""
        graph_index = 1
        vertex_index = 1
        G = nx.Graph()
        with open(self.ds_gi, "r") as fp_gi:
            for gi in fp_gi:
                if int(gi) > graph_index:
                    yield G, vertex_index
                    graph_index = int(gi)
                    G = nx.Graph()
                G.add_node(vertex_index)
                vertex_index = vertex_index + 1
            yield G, vertex_index

    def read_graph(self):
        """Reads the edges from file 'A' which contains the adjacency list. Returns the graph as networkX oject"""
        graphs = self._read_vertices()
        G, next_node = next(graphs)
        with open(self.ds_a, "r") as fp_a:  # , open(self.ds_el, "r") as fp_el:
            for edges in fp_a:  # , labels in zip(fp_a, fp_el):
                start, end = [int(e.strip()) for e in edges.split(',')]
                if start >= next_node:
                    yield G
                    try:
                        G, next_node = next(graphs)
                    except StopIteration:
                        return
                G.add_edge(start, end)  # ,bond_type=int(labels))
            yield (G)

    def read_label(self):
        """Reads the label of a graph from label file and returns it."""
        with open(self.ds_gl, "r") as fp_gl:
            for gl in fp_gl:
                yield int(gl)


def padded_linearized_adj(G, max_size):
    """Returns the concatinated rows of the upper triangular matrix of the adjacency matrix.
    The output vector is padded with zeros up to max_size"""
    pad_size = max_size - len(G)
    A = nx.to_numpy_matrix(G)
    pA = np.pad(A, [(0, pad_size), (0, pad_size)], 'constant', constant_values=[(0, 0), (0, 0)])

    return pA[np.triu_indices(max_size, 0)]


def graphs_from_datasets(namelist, directory):
    """Returns list of graphs out of list of data sets (namelist) from the given directory. """
    graph_list = []
    max_size = 0

    for name in namelist:
        p = Parser(name, directory)
        graphs = p.read_graph()
        for G in graphs:
            graph_list.append(G)
            if len(G) > max_size:
                max_size = len(G)

    return graph_list, max_size


def labels_from_datasets(namelist, directory):
    """Returns list of labels out of list of data sets (namelist) from the given directory."""
    label_list = []

    for name in namelist:
        p = Parser(name, directory)
        labels = p.read_label()
        for l in labels:
            label_list.append(l)

    return label_list


def data(namelist, directory):
    """Returns list of graphs and corresponding labels out of list of data sets (namelist) from the given directory.
    The graphs are linearized and padded"""
    graphs, max_size = graphs_from_datasets(namelist, directory)
    lingraphs = np.array([padded_linearized_adj(G, max_size) for G in graphs])
    labels = np.array([l for l in labels_from_datasets(namelist, directory)])
    return lingraphs, labels


if __name__ == '__main__':
    # Variant 1:
    p = Parser("DD", ".")
    graphs = p.read_graph()
    for graph in graphs:
        for v in graph.nodes():
            print(v, ":", list(graph.neighbors(v)))
        print()

    # Variant 2:
    graphs, m = graphs_from_datasets(["DD"], ".")

    for graph in graphs:
        for v in graph.nodes():
            print(v, ":", list(graph.neighbors(v)))
        print()
