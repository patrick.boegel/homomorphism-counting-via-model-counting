import networkx as nx
from networkx.algorithms.approximation import treewidth_min_degree
from GraphParser.datasets import Parser


class filtered_Parser:  
    
    def __init__(self, data_set, path, size=0, width=0):
        self.Parser = Parser(data_set, path)
        self.size = size
        self.width = width
                
    def read_graph(self):
        graphs = self.Parser.read_graph()
        
        for G in graphs:
            if self.size==0 or len(G.nodes) <= self.size:
                treewidth, _ = treewidth_min_degree(G)
                if self.width==0 or treewidth <= self.width:
                    yield G
        return
    
    
    

class size_filtered_Parser:    
    def __init__(self, data_set, path, size= 50):
        self.Parser = Parser(data_set, path)
        self.size = size        

    def read_graph(self):
        graphs = self.Parser.read_graph()
        
        for G in graphs:
            if len(G.nodes) <= self.size:
                yield G
        return

    
    
class width_filtered_Parser:    
    def __init__(self, data_set, path, width= 2):
        self.Parser = Parser(data_set, path)
        self.width = width        

    def read_graph(self):
        graphs = self.Parser.read_graph()
        
        for G in graphs:
            treewidth, decomposition = treewidth_min_degree(G)
            if treewidth <= self.width:
                yield G
        return
        
        

def read_graph_widthFilted(data_set , path , width = 2):
    p = filteredParser(data_set, path, width)
    return ( G for G in p.read_graph() )





    

if __name__ == '__main__':
    
    graphs = read_graph_widthFilted("Peking_1", "GraphParser") #Peking_1

    i = 0;
    for G in graphs:
        treewidth, decomposition = treewidth_min_degree(G)
        #print(treewidth, end=' , ')
        i += 1
    print(i)