import networkx as nx
import createDNNF
from GraphParser.graphFilter import filtered_Parser
import homomorphism as hom
import GraphParser.caterpillar as cp
import re

import sys            # to read in arguments
import subprocess     # to execute model counters
import time           # to measure time
from pathlib import Path  #to create output directories

# --- create DNNF-files ---

lineWidth = 75 # to overwrite complete lines

def create_DNNF_files(writeFunc,GParser,HParser,dnnfFiles):
    #writeFunc: function for writing each dnnf file
    HGraphs = HParser.read_graph()
    i = 1
    ctime = 0
    for H in HParser.read_graph():
        j = 1
        for G in GParser.read_graph():
            start = time.time()
            writeFunc(H, G, dnnfFiles.format(i,j))
            ctime += time.time()-start
            j += 1
        i += 1
    print('Creating DNNF-files: ' + str(ctime) + 's')
    return

# --- execute model counter ---

def solve_all_dnnfs(solverFunc,dnnfFolder,countFile):
    p = Path(dnnfFolder)
    files = get_files_in_folder(p,'.nnf')
    
    ctime = 0
    with open(countFile,'w') as cF:
        try:
            for file in files:
                ind = re.search('\d+_\d+', file).group().split('_') # find XX_YY in file name (with XX,YY numbers)
                try:
                    start = time.time()
                    homCount = solverFunc( str(p / file) )
                except subprocess.CalledProcessError as e:
                    homCount = '-error-'
                    if not e.stderr==None:
                        print('\r'.ljust(lineWidth)) #clear line
                        print('\r{i:d} {j:d} : {msg:s}'.format(i=int(ind[0]), j=int(ind[1]),msg=e.stderr))
                end = time.time()
                line = '{i:d} {j:d} : {sol:s} t={t:.3f}s'.format(
                    i=int(ind[0]), j=int(ind[1]), sol=homCount, t=end-start)
                ctime += end-start
                print(('\r'+ line +' T={:.1f}s'.format(ctime)).ljust(lineWidth) ,end='') # T = total time
                cF.write(line +'\n')
            print('\r',end='')
        except KeyboardInterrupt: #model counter aborted
            end = time.time()
            ctime += end-start
            print( '\rCalculation aborted during ({i:d},{j:d})'.format(i=int(ind[0]), j=int(ind[1]) ).ljust(lineWidth) )
            cF.write( 'Aborted at t={t:.2f}s, T={T:.2f}s\n'.format(t=end-start, T=ctime) )
        except:
            end = time.time()
            ctime += end-start
            cF.write( 'Error at t={t:.2f}s, T={T:.2f}s\n'.format(t=end-start, T=ctime) )
            raise
            
    #endWith (closes file)
    end = time.time()
    print( ('Counting Models: '+ str(ctime) +'s').ljust(lineWidth) )
    
    
def solve_all_dnnfs_skippable(solverFunc,dnnfFolder,countFile):
    p = Path(dnnfFolder)
    files = get_files_in_folder(p,'.nnf')
    
    ctime = 0
    with open(countFile,'w') as cF:
        try:
            for file in files:
                ind = re.search('\d+_\d+', file).group().split('_') # find XX_YY in file name (with XX,YY numbers)
                try:
                    start = time.time()
                    homCount = solverFunc( str(p / file) )
                except subprocess.CalledProcessError as e:
                    homCount = '-error-'
                    if not e.stderr==None:
                        print('\r'.ljust(lineWidth)) #clear line
                        print('\r{i:d} {j:d} : {msg:s}'.format(i=int(ind[0]), j=int(ind[1]),msg=e.stderr))
                except KeyboardInterrupt:
                    homCount = '-aborted-'
                    if time.time()-start < 0.1: # Ctrl-C held down
                        raise
                end = time.time()
                line = '{i:d} {j:d} : {sol:s} t={t:.3f}s'.format(
                    i=int(ind[0]), j=int(ind[1]), sol=homCount, t=end-start)
                ctime += end-start
                print('\r'+ line +' T={:.1f}s'.format(ctime).ljust(lineWidth) ,end='') # T = total time
                cF.write(line +'\n')
            print('\r',end='')
        except KeyboardInterrupt: #model counter aborted
            end = time.time()
            ctime += end-start
            print( '\rCalculation aborted during ({i:d},{j:d})'.format(i=int(ind[0]), j=int(ind[1]) ).ljust(lineWidth) )
            cF.write( 'Aborted at t={t:.2f}s, T={T:.2f}s\n'.format(t=end-start, T=end-start) )
        except:
            end = time.time()
            ctime += end-start
            cF.write( 'Error at t={t:.2f}s, T={T:.2f}s\n'.format(t=end-start, T=ctime) )
            raise
            
    #endWith (closes file)
    print( ('Counting Models: '+ str(ctime) +'s').ljust(lineWidth) )

def solver_query_dnnf( dnnfFile ):
    pipe = subprocess.Popen( ['query-dnnf'], stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE, stderr=subprocess.PIPE ,encoding='utf-8')
    out,err = pipe.communicate(input=('load '+dnnfFile +'\nmc'))
    solInd = out.find('\n> ') +3
    if solInd == 1 :
        raise subprocess.CalledProcessError(pipe.returncode, ['query-dnnf'], stderr=err)
    return out[solInd:out.find('\n',solInd)]

def solver_python( dnnfFile ):
    with open(dnnfFile,'r') as f:
        vals = []
        for line in f:
            if len(line)==0:
                pass
            elif line[0] == 'A':
                refs = line[2:].split(' ')
                val = 1
                for ref in refs[1:]: #skip n 
                    val *= vals[int(ref)]
                vals += [val]
            elif line[0] == 'O':
                refs = line[2:].split(' ')
                val = 0
                for ref in refs[2:]: #skip n,i
                    val += vals[int(ref)]
                vals += [val]
            elif line[0] == 'L':
                vals += [1]
        return str(vals[-1])
            

# list dnnf-files in numerical order
def get_files_in_folder(path, suffix):
    files = [f.name   for f in path.iterdir()   if f.suffix == suffix ]
    files = sorted(files, key = numericalSort)
    return files
    
numbers = re.compile(r'(\d+)')
def numericalSort(value): #e.g. value='Test13_from_210' -> parts=['Test', 13 , '-from_', 210]
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts



# -- functions to count homomorphisms --

def get_write_func(phiVersion): 
    if phiVersion == 'dyn':
        writeFunc = createDNNF.createPhi_dyn
    elif phiVersion == 'dyn_full':
        writeFunc = createDNNF.createPhi_dyn_full
    else:
        writeFunc = None
        
    return writeFunc
        
    

def get_solver_function(solver):
    if solver == 'query-dnnf':
        mc_Func = solver_query_dnnf
    elif solver == 'python':
        mc_Func = solver_python
    else:
        print(solver)
        mc_Func = None
    
    return mc_Func
    


def count_with_algorithm(GParser,HParser,name):  
    countFile = 'Runtime/'+ name +'_counts.txt'
    
    ctime = 0    
    i = 1
    with open(countFile,'w') as cF:
    
        for H in HParser.read_graph():
            j = 1
            for G in GParser.read_graph():
                start = time.time()
                h = hom.count_from_bounded_treewidth(H, G)
                end = time.time()
                line = '{i:d} {j:d} : {sol:d} t={t:.3f}s\n'.format(
                    i=i, j=j, sol=h, t=end-start)
                cF.write(line)
                ctime += end-start
                j += 1
            i += 1

    #endWith (closes file)
    print('Counting Models: '+ str(ctime) +'s')


# --- main function ---

if __name__ == '__main__':
    
    
    #name = 'RedTreePek'    
    #HParser = filtered_Parser('REDDIT-BINARY', 'GraphParser', 20, 1) #dataset, folder, [size], [width]   
    #GParser = filtered_Parser('Peking_1', 'GraphParser', 20)
    
    name = 'caterpillars30'    
    HParser = cp.caterpillar_Parser(30)
    GParser = cp.graph_AIDS11_Parser()    
    
    #print(sum([1 for H in HParser.read_graph()]))
    
    # --- run using model counters on default formulas  ---
    if len(sys.argv)>=3:  #default: argv = [ 'main.py', solver, phiVersion,[skippable] ]
        
        mc = sys.argv[1]
        
        writeFunc = get_write_func(sys.argv[2])
        if not writeFunc == None: 
            # -- create DNNF files --
            phiVersion = sys.argv[2]
            dnnfFolder = 'OutputCNF/{name:s}_phi_{phi:s}'.format(name=name,phi = phiVersion)
            dnnfFiles = dnnfFolder +'/'+ name +'{}_{}.nnf'

            Path(dnnfFolder).mkdir(parents=True, exist_ok=True) #create dnnf-folder if it doesn't exist

            create_DNNF_files(writeFunc,GParser,HParser,dnnfFiles)
            
            countFile = 'Runtime/{name:s}_phi_{phi:s}_{mc:s}_counts.txt'.format(name=name , phi=phiVersion, mc=mc)
                    
        else:
            # --read in folder --
            if not Path(sys.argv[2]).is_dir:
                print('Unknown phi version / folder')
                sys.exit(0)
                
            dnnfFolder = sys.argv[2]
            if dnnfFolder[-1] == '/':
                dnnfFolder = dnnfFolder[:-1]
            folder = dnnfFolder.split('/')[-1] # top-level folder in the path 
            
            countFile = 'Runtime/{folder:s}_{mc:s}_counts.txt'.format(folder=folder , mc=mc)        
        
        
        mc_Func = get_solver_function(mc)
        if mc_Func == None:
            print('Unknown Model Counter')
            sys.exit(0)
        
            
        # -- run solver --
        processes = subprocess.check_output(['ps','-A'])
        if( bytes('mem_warn.sh','utf8') not in processes ):
            subprocess.call('gnome-terminal --title "Memory Observer" -- "mem_warn.sh"', shell=True)
        
        if len(sys.argv)>3 and (sys.argv[3]=='1' or sys.argv[3]=='True'): #skippable option
            solve_all_dnnfs_skippable( mc_Func ,dnnfFolder, countFile )
        else:
            solve_all_dnnfs( mc_Func ,dnnfFolder, countFile )
        
    
    
    # --- runs the algorithm from masterthesis ---
    elif len(sys.argv)==2 and sys.argv[1]=='algo': # tree-width<=2 [masterthesis 6.2, p.62]
        
        count_with_algorithm(GParser,HParser,name)
    
        
        
    else:
        print('Unknown arguments:')
        
#end main function




