# -*- coding: utf-8 -*-

#    Copyright (C) 2019 by
#    Maximilian Merz <maximilian.merz@rwth-aachen.de>
#    All rights reserved.
#    BSD license.

"""Functions for counting homomorphisms between graphs."""

import itertools

import networkx as nx
from networkx.algorithms.approximation import treewidth_min_degree

__author__ = "\n".join(["Maximilian Merz (maximilian.merz@rwth-aachen.de)"])

__all__ = [
    "count_from_bounded_treewidth",
    "count_from_bounded_treewidth0",
    "count_from_bounded_treewidth1",
    "count_from_bounded_treewidth2",
    "count_from_bounded_treewidth3",
]


def count_from_bounded_treewidth(H, G):
    """Return the number of homomorphisms between the two graphs.

    Parameters
    ----------
    H, G : graphs
       The two graphs H and G must be the same type.
       The graph H should have small treewidth.

    Returns
    -------
    h : integer
        The return value `h` is the number of homomorphisms from H to G.

    """
    counter = DiazSernaThilikosCounter(H, G)
    return counter.count()


def count_from_bounded_treewidth3(H, G):
    """Return the number of homomorphisms between the two graphs.

    Returns an integer representing the number of homomorphisms from
    graph H to graph G. H should have small treewidth.

    Notes
    -----
    This is an alternate function name for the current implementation.

    """
    counter = DiazSernaThilikosCounter3(H, G)
    return counter.count()


def count_from_bounded_treewidth2(H, G):
    """Return the number of homomorphisms between the two graphs.

    Returns an integer representing the number of homomorphisms from
    graph H to graph G. H should have small treewidth.

    Notes
    -----
    This is an alternate implementation and expected to perform worse.
    It differs in its implementation of the `_forget_node` subroutine.

    """
    counter = DiazSernaThilikosCounter2(H, G)
    return counter.count()


def count_from_bounded_treewidth1(H, G):
    """Return the number of homomorphisms between the two graphs.

    Returns an integer representing the number of homomorphisms from
    graph H to graph G. H should have small treewidth.

    Notes
    -----
    This is an alternate implementation and expected to perform worse.
    It differs in that it saves homomorphism numbers of 0 explicitly to the
    table. The “better” implementation does not save 0's and checks before
    every access to the table whether the mapping exists in the table:

    Before:
        table[phi]

    After:
        if phi in table:
            table[phi]
        else:
            0  # or do nothing

    """
    counter = DiazSernaThilikosCounter1(H, G)
    return counter.count()


def count_from_bounded_treewidth0(H, G):
    """Return the number of homomorphisms between the two graphs.

    Returns an integer representing the number of homomorphisms from
    graph H to graph G. H should have small treewidth.

    Notes
    -----
    This is an alternate implementation and expected to perform worse.
    It differs in that it iterates over all nodes of `G` in the
    `_introduce_node` subroutine. The “better” implementation calculates the
    nodes that fulfill the homomorphism condition and iterates only over those.

    """
    counter = DiazSernaThilikosCounter0(H, G)
    return counter.count()


class DiazSernaThilikosCounter:
    table = dict()
    H = None
    decomposition = None
    G = None

    def __init__(self, H, G):
        self.H = H
        self.G = G

        # Compute a treewidth decomposition.
        treewidth, decomposition = treewidth_min_degree(H)
        # TODO treewidth, decomposition = treewidth_min_fill_in(H)

        decomposition = nx.relabel.convert_node_labels_to_integers(
            decomposition, label_attribute="bag",
        )

        # Pick any node as root node.
        root = list(decomposition)[0]
        self.root = root

        # Get an oriented tree constructed from a depth-first-search from
        # `root`.
        # TODO: We need to keep the frozensets as part of our decomposition,
        #       but nx.dfs_tree removes them. As a quick fix, we manually add
        #       them back later, but there must be a better way to do this.
        #       (See also: https://github.com/networkx/networkx/issues/411)
        old_decomp = decomposition
        decomposition = nx.dfs_tree(old_decomp, root)
        for node in decomposition:
            decomposition.add_node(node, **old_decomp.nodes[node])

        # TODO: Convert to a nice tree decomposition.
        # nice_dec = nx.algorithms.treewidth.convert_to_nice(decomposition)

        self.decomposition = decomposition

    def _bag_ordering(self):
        """Compute a postorder traversal of the tree decomposition's bags."""
        return nx.dfs_postorder_nodes(self.decomposition, self.root)

    def _all_mappings(self, from_nodes):
        """Yield all possible mappings of `from_nodes` to `self.G`.

        Generator that yields all possible mappings from the vertices in
        `bag` (a subset of the vertices of `self.H`) onto the vertices of
        `self.G`. Corresponds to the set `F_p` as defined in the paper.
        """
        from_nodes = tuple(from_nodes)
        mappings = itertools.product(self.G.nodes, repeat=len(from_nodes))
        for mapping in mappings:
            yield zip(from_nodes, mapping)

    def _beta(self, bag):
        return self._get_nodes_in(bag)

    def _get_nodes_in(self, bag):
        return self.decomposition.nodes[bag]["bag"]

    def count(self):
        # Iterate over all bags of the decomposition in the selected ordering.
        for bag in self._bag_ordering():
            # Start nodes.
            if self.decomposition.out_degree(bag) == 0:
                self.table[bag] = self._process_start_bag(self._beta(bag))

            # Internal nodes.
            else:
                bag_table = None

                # Find the bag's children.
                children = self.decomposition.successors(bag)
                i = 0

                for child in children:

                    child_set = self._beta(child)
                    parent_set = self._beta(bag)

                    # Forget nodes.
                    intermediate_table = self._forget_nodes(
                        child, bag, child_set, parent_set
                    )

                    # We now work with the implicit child set instead of the
                    # "real" one, because we already "forgot" all nodes not in
                    # the parent set.
                    implicit_child_set = self._beta(child) & self._beta(bag)

                    # Introduce nodes.
                    table = self._introduce_nodes(
                        child, bag, implicit_child_set, parent_set,
                        intermediate_table
                    )

                    # Join operation.
                    # Merge the table for this child into the `bag_table`.
                    if bag_table is None:
                        # Initial case of this being the first child.
                        bag_table = table
                    else:
                        # We merge `table` into `bag_table`.
                        for phi in list(bag_table):
                            if phi in table:
                                bag_table[phi] *= table[phi]
                            else:
                                # table[phi] is (implicitly) 0.
                                del bag_table[phi]

                    i = i + 1

                self.table[bag] = bag_table

        hom = self._process_root_bag(self._beta(self.root))

        # Return the value we computed for the root node.
        return hom

    def _process_start_bag(self, start_set):
        """Process a start bag.

        Introduce all its nodes and return the resulting dynamic programming
        table.
        """
        child_set = parent_set = frozenset()
        child_table = parent_table = dict()

        for u in start_set:
            parent_set = parent_set | {u}

            if len(child_set) == 0:
                # first one, act like start bag
                for v in self.G:
                    phi = frozenset({(u, v)})
                    parent_table[phi] = 1
            else:
                # act like an introduce
                parent_table = self._introduce_node(
                    u, child_set, parent_set, child_table
                )

            child_set = parent_set
            child_table = parent_table

        return parent_table

    def _introduce_node(self, u, child_set, parent_set, child_table):
        """Introduce node `u`.

        Return the dynamic programming table for the bag that introduces
        node u.

        Parameters
        ----------
        u :
            The node u in `self.H` that is introduced.

        child_set : set
            The set that corresponds to the child bag, it contains those
            nodes of `self.H` that the child bag contains.

        parent_set : set
            The set that corresponds to the parent bag, it contains those
            nodes of `self.H` that the parent bag contains. These are all
            nodes in `child_set` plus the node `u`.

        child_table : dict
            This is the dynamic programming table that has been computed
            for the child bag.

        Returns
        ------
        table : dict
            This is the dynamic programming table for the parent bag.

        """
        # Initialise the result table.
        table = dict()

        # S_child is defined as the set of all nodes in the child bag that are
        # also neighbors of `u` (the node being introduced) in `self.H`.
        S_child = child_set & set(self.H.neighbors(u))

        # We iterate over all possible mappings between the child set and the
        # nodes of self.G.
        for phi in self._all_mappings(child_set):
            # `phi` is an iterator, in order to use it more than once, we turn
            # it into a frozenset - order does not interest us.
            phi = frozenset(phi)

            # We check whether `child_table[phi]` is “implicitly” 0.
            if phi not in child_table:
                # It is. There are no extensions of phi possible. We do nothing
                # for this mapping and continue to the next.
                continue

            # We calculate the set phi(S_child). S_child is defined above.
            # phi(S_child) is the image of this set under the mapping phi. This
            # image is a subset of the nodes of `self.G`.
            phi_of_S_child = [v for (u, v) in phi if u in S_child]

            # We now calculate the “common neighbors”. This is a node set in
            # `self.G` and consists of those nodes v that are neighbor to all
            # nodes in phi(S_child).
            # Note that the nodes in this set are exactly the nodes v that
            # fulfill the homomorphism condition when the mapping phi is
            # extended by u -> v.
            common_neighbors = frozenset(self.G.nodes)
            for v in phi_of_S_child:
                common_neighbors = \
                    common_neighbors & frozenset(self.G.neighbors(v))

            # For each node v in V(G), we want to extend the mapping `phi` by
            # phi(u) -> v and add the homomorphism number for the resulting
            # mapping `phi_ext` to the table. If v is in the set of common
            # neighbours, then this is equal to the number of homomorphisms
            # for `phi` in the child bag's table. Else, it is zero.
            for v in common_neighbors:
                phi_ext = phi | {(u, v)}
                table[phi_ext] = child_table[phi]

        return table

    def _introduce_nodes(
        self,
        final_child,
        final_parent,
        final_child_set,
        final_parent_set,
        intermediate_table,
    ):
        """Introduce the nodes that were not be part of the final child bag."""
        introduced = final_parent_set - final_child_set
        # Create tables.
        parent_table = child_table = intermediate_table
        # Create intermediate sets.
        parent_set = child_set = final_child_set

        for u in introduced:
            parent_set = parent_set | {u}

            parent_table = self._introduce_node(
                u, child_set, parent_set, child_table
            )

            child_set = parent_set
            child_table = parent_table

        return parent_table

    def _forget_node(self, u, child_set, parent_set, child_table):
        """Forget node `u`."""
        # Initialize table for the “implicit” parent bag.
        table = dict()

        # Iterate over all keys in the `child_table`.
        for phi_ext in child_table:
            # Remove u from `phi_ext` to get a mapping `phi`.
            phi_ext_dict = dict(phi_ext)
            del phi_ext_dict[u]
            phi = frozenset(phi_ext_dict.items())
            # Update the table.
            if phi in table:
                table[phi] += child_table[phi_ext]
            else:
                table[phi] = child_table[phi_ext]

        return table

    def _forget_nodes(
        self, final_child, final_parent, final_child_set, final_parent_set
    ):
        """Forget the nodes that will not be part of the final parent bag.

        Computes the table entries for an “implicit parent” node between
        `child` and `bag` that has the nodes that are in the intersection
        of the `child` and `parent` bags.
        Instead of transforming our tree decomposition into a nice tree
        decomposition, we want to do this “implicitly”. We iterate over all
        forgotten nodes one by one and set `implicit_child` and
        `implicit_parent` to intermediate parent and child bag sets that
        each only “forget” a single node. On each of these, we call
        `_forget_node`.
        """
        forgotten_nodes = final_child_set - final_parent_set
        # Create intermediate tables.
        child_table = self.table[final_child]
        del self.table[final_child]
        parent_table = None
        # Create intermediate sets.
        child_set = parent_set = final_child_set

        for u_forget in forgotten_nodes:
            parent_set = parent_set - {u_forget}

            parent_table = self._forget_node(
                u_forget, child_set, parent_set, child_table
            )

            child_set = parent_set
            child_table = parent_table

        return parent_table

    def _process_root_bag(self, root_set):
        """Process the root bag of the tree decomposition.

        Forgets all nodes in the root bag and then returns the value that the
        dynamic programming table holds for the empty mapping.
        """
        implicit_child_set = implicit_parent_set = root_set
        child_table = self.table[self.root]
        del self.table[self.root]
        parent_table = None

        for u in root_set:
            implicit_parent_set = implicit_parent_set - {u}

            parent_table = self._forget_node(
                u, implicit_child_set, implicit_parent_set, child_table
            )

            implicit_child_set = implicit_parent_set
            child_table = parent_table

        null_mapping = frozenset()
        if null_mapping in parent_table:
            return parent_table[null_mapping]
        else:
            return 0


class DiazSernaThilikosCounter3(DiazSernaThilikosCounter):
    pass


class DiazSernaThilikosCounter2(DiazSernaThilikosCounter3):
    def _forget_node(self, u, child_set, parent_set, child_table):
        """Forget node `u`."""
        # Initialize table for the “implicit” parent bag.
        table = dict()

        # Iterate over all possible mappings.
        for phi in self._all_mappings(parent_set):
            phi = frozenset(phi)
            acc = 0
            for v in self.G:
                phi_ext = phi | {(u, v)}
                if phi_ext in child_table:
                    acc += child_table[phi_ext]
            if acc > 0:
                table[phi] = acc

        return table


class DiazSernaThilikosCounter1(DiazSernaThilikosCounter2):
    def count(self):
        # Iterate over all bags of the decomposition in the selected ordering.
        for bag in self._bag_ordering():
            # Start nodes.
            if self.decomposition.out_degree(bag) == 0:
                self.table[bag] = self._process_start_bag(self._beta(bag))

            # Internal nodes.
            else:
                bag_table = dict()

                # Find the bag's children.
                children = self.decomposition.successors(bag)
                i = 0

                for child in children:

                    child_set = self._beta(child)
                    parent_set = self._beta(bag)

                    # Forget nodes.
                    intermediate_table = self._forget_nodes(
                        child, bag, child_set, parent_set
                    )

                    # We now work with the implicit child set instead of the
                    # “real” one, because we already “forgot” all nodes not in
                    # the parent set.
                    implicit_child_set = self._beta(child) & self._beta(bag)

                    # Introduce nodes.
                    table = self._introduce_nodes(
                        child, bag, implicit_child_set, parent_set,
                        intermediate_table
                    )

                    # Join operation.
                    # Merge the table for this child into the `bag_table`.
                    if len(bag_table) == 0:
                        bag_table = table
                    else:
                        for phi in bag_table:
                            bag_table[phi] *= table[phi]

                    i = i + 1

                self.table[bag] = bag_table

        hom = self._process_root_bag(self._beta(self.root))

        # Return the value we computed for the root node.
        return hom

    def _introduce_node(self, u, child_set, parent_set, child_table):
        """Introduce node `u`.

        Returns the dynamic programming table for the bag that introduces
        node u.

        Parameters
        ----------
        u :
            The node u in `self.H` that is introduced.

        child_set : set
            The set that corresponds to the child bag, it contains those
            nodes of `self.H` that the child bag contains.

        parent_set : set
            The set that corresponds to the parent bag, it contains those
            nodes of `self.H` that the parent bag contains. These are all
            nodes in `child_set` plus the node `u`.

        child_table : dict
            This is the dynamic programming table that has been computed
            for the child bag.

        Returns
        ------
        table : dict
            This is the dynamic programming table for the parent bag.

        """
        # Initialise the result table.
        table = dict()

        # S_child is defined as the set of all nodes in the child bag that are
        # also neighbors of `u` (the node being introduced) in `self.H`.
        S_child = child_set & set(self.H.neighbors(u))

        # We iterate over all possible mappings between the child set and the
        # nodes of self.G.
        for phi in self._all_mappings(child_set):
            # `phi` is an iterator, in order to use it more than once, we turn
            # it into a frozenset - order does not interest us.
            phi = frozenset(phi)

            # We calculate the set phi(S_child). S_child is defined above.
            # phi(S_child) is the image of this set under the mapping phi. This
            # image is a subset of the nodes of `self.G`.
            phi_of_S_child = [v for (u, v) in phi if u in S_child]

            # We now calculate the “common neighbors”. This is a node set in
            # `self.G` and consists of those nodes v that are neighbor to all
            # nodes in phi(S_child).
            # Note that the nodes in this set are exactly the nodes v that
            # fulfill the homomorphism condition when the mapping phi is
            # extended by u -> v.
            common_neighbors = frozenset(self.G.nodes)
            for v in phi_of_S_child:
                common_neighbors = \
                    common_neighbors & frozenset(self.G.neighbors(v))

            # We also compute the complement of the “common neighbours”.
            not_common_neighbors = frozenset(self.G.nodes) - common_neighbors

            # For each node v in V(G), we want to extend the mapping `phi` by
            # phi(u) -> v and add the homomorphism number for the resulting
            # mapping `phi_ext` to the table. If v is in the set of common
            # neighbours, then this is equal to the number of homomorphisms
            # for `phi` in the child bag's table. Else, it is zero.
            for v in common_neighbors:
                phi_ext = phi | {(u, v)}
                table[phi_ext] = child_table[phi]

            for v in not_common_neighbors:
                phi_ext = phi | {(u, v)}
                table[phi_ext] = 0

        return table

    def _forget_node(self, u, child_set, parent_set, child_table):
        """TODO document _forget_node."""
        # Initialize table for the “implicit” parent bag.
        table = dict()

        # Iterate over all possible mappings.
        for phi in self._all_mappings(parent_set):
            phi = frozenset(phi)
            acc = 0
            for v in self.G:
                phi_ext = phi | {(u, v)}
                if phi_ext in child_table:
                    acc += child_table[phi_ext]
            table[phi] = acc

        return table


class DiazSernaThilikosCounter0(DiazSernaThilikosCounter1):
    def _introduce_node(self, u, child_set, parent_set, child_table):
        """Naive implementation of `_introduce_node`.

        This version iterates over all possible mappings (inefficient). This
        code follows closely the formulation in the paper.
        """
        S_child = child_set & set(self.H.neighbors(u))
        table = dict()

        for phi in self._all_mappings(child_set):
            phi = frozenset(phi)
            phi_dict = dict(phi)
            for v in self.G.nodes:
                # We want to extend the mapping phi by phi(u)=v.
                # Test homomorphism condition.
                hom_condition_satisfied = True
                for u_n in S_child:
                    if not self.G.has_edge(phi_dict[u_n], v):
                        hom_condition_satisfied = False
                        break

                # Update the table accordingly.
                phi_ext = phi | {(u, v)}
                if hom_condition_satisfied:
                    table[phi_ext] = child_table[phi]
                else:
                    table[phi_ext] = 0

        return table
