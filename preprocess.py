import subprocess
import sys
import time
from pathlib import Path

if __name__ == '__main__':
    
    if len(sys.argv)>=2:
        inFolder = sys.argv[1]  #argument 1: input folder
    else:
        inFolder = './'
    if len(sys.argv)>=3:
        outFolder = sys.argv[2] #argument 2: output folder
    else:
        outFolder = inFolder
        
    if not inFolder == outFolder:
        outSuffix = '.cnf'
    else:
        outSuffix = '_pp.cnf'
    
    pOut = Path(outFolder)
    pIn = Path(inFolder)
    files = [f.name   for f in pIn.iterdir()   if f.suffix =='.cnf' ]
    
    pOut.mkdir(parents=True, exist_ok=True) #create out-folder if it doesn't exist
    
    start = time.time()
    
    for file in files:
        
        with open( (str(pOut / file[:-4] )+outSuffix), 'w') as outF: 
            
            subprocess.call(['pmc','-vivification','-eliminateLit','-litImplied','-iterate=10',
                             '-equiv','-orGate','-affine','-verb=0',str(pIn / file)], stdout=outF)
        
    end = time.time()    
    print( 'Preprocessing: ' + str(end - start) + 's')