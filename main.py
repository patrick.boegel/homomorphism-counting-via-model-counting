import networkx as nx
import createPhi
import createPhi_approx
from GraphParser.graphFilter import filtered_Parser
import homomorphism as hom
import re

import sys            # to read in arguments
import subprocess     # to execute model counters
import time           # to measure time
from pathlib import Path  #to create output directories

# --- create CNF-files ---

lineWidth = 75 # to overwrite complete lines

def create_CNF_files(writeFunc,GParser,HParser,cnfFiles):
    #writeFunc: function for writing each cnf file
    HGraphs = HParser.read_graph()
    i = 1
    ctime = 0
    for H in HParser.read_graph():
        j = 1
        for G in GParser.read_graph():
            start = time.time()
            writeFunc(H, G, cnfFiles.format(i,j))
            ctime += time.time()-start
            j += 1
        i += 1
    print('Creating Cnf-files: ' + str(ctime) + 's')
    return

# --- execute model counter ---

def solve_all_cnfs(solverFunc,cnfFolder,countFile):
    p = Path(cnfFolder)
    files = get_files_in_folder(p,'.cnf')
    
    ctime = 0
    with open(countFile,'w') as cF:
        try:
            for file in files:
                ind = re.search('\d+_\d+', file).group().split('_') # find XX_YY in file name (with XX,YY numbers)
                try:
                    start = time.time()
                    homCount = solverFunc( str(p / file) )
                except subprocess.CalledProcessError as e:
                    homCount = '-error-'
                    if not e.stderr==None:
                        print('\r'.ljust(lineWidth)) #clear line
                        print('\r{i:d} {j:d} : {msg:s}'.format(i=int(ind[0]), j=int(ind[1]),msg=e.stderr))
                end = time.time()
                line = '{i:d} {j:d} : {sol:s} t={t:.2f}s'.format(
                    i=int(ind[0]), j=int(ind[1]), sol=homCount, t=end-start)
                ctime += end-start
                print(('\r'+ line +' T={:.1f}s'.format(ctime)).ljust(lineWidth) ,end='') # T = total time
                cF.write(line +'\n')
            print('\r',end='')
        except KeyboardInterrupt: #model counter aborted
            end = time.time()
            ctime += end-start
            print( '\rCalculation aborted during ({i:d},{j:d})'.format(i=int(ind[0]), j=int(ind[1]) ).ljust(lineWidth) )
            cF.write( 'Aborted at t={t:.1f}s, T={T:.1f}s\n'.format(t=end-start, T=ctime) )
        except:
            end = time.time()
            ctime += end-start
            cF.write( 'Error at t={t:.1f}s, T={T:.1f}s\n'.format(t=end-start, T=ctime) )
            raise
            
    #endWith (closes file)
    end = time.time()
    print( ('Counting Models: '+ str(ctime) +'s').ljust(lineWidth) )
    
    
def solve_all_cnfs_skippable(solverFunc,cnfFolder,countFile):
    p = Path(cnfFolder)
    files = get_files_in_folder(p,'.cnf')
    
    ctime = 0
    with open(countFile,'w') as cF:
        try:
            for file in files:
                ind = re.search('\d+_\d+', file).group().split('_') # find XX_YY in file name (with XX,YY numbers)
                try:
                    start = time.time()
                    homCount = solverFunc( str(p / file) )
                except subprocess.CalledProcessError as e:
                    homCount = '-error-'
                    if not e.stderr==None:
                        print('\r'.ljust(lineWidth)) #clear line
                        print('\r{i:d} {j:d} : {msg:s}'.format(i=int(ind[0]), j=int(ind[1]),msg=e.stderr))
                except KeyboardInterrupt:
                    homCount = '-aborted-'
                    if time.time()-start < 0.1: # Ctrl-C held down
                        raise
                end = time.time()
                line = '{i:d} {j:d} : {sol:s} t={t:.2f}s'.format(
                    i=int(ind[0]), j=int(ind[1]), sol=homCount, t=end-start)
                ctime += end-start
                print('\r'+ line +' T={:.1f}s'.format(ctime).ljust(lineWidth) ,end='') # T = total time
                cF.write(line +'\n')
            print('\r',end='')
        except KeyboardInterrupt: #model counter aborted
            end = time.time()
            ctime += end-start
            print( '\rCalculation aborted during ({i:d},{j:d})'.format(i=int(ind[0]), j=int(ind[1]) ).ljust(lineWidth) )
            cF.write( 'Aborted at t={t:.1f}s, T={T:.1f}s\n'.format(t=end-start, T=end-start) )
        except:
            end = time.time()
            ctime += end-start
            cF.write( 'Error at t={t:.1f}s, T={T:.1f}s\n'.format(t=end-start, T=ctime) )
            raise
            
    #endWith (closes file)
    print( ('Counting Models: '+ str(ctime) +'s').ljust(lineWidth) )

def solver_countAntom( cnfFile ):
    pipe = subprocess.Popen( ['countAntom',cnfFile], stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE ,encoding='utf-8') #circumvent exit code of 10
    out,err = pipe.communicate()
    solution_index = out.find('c model count............: ')+27
    if solution_index==26: #not found (=-1+15)
        if err[0] == '\n':
            err = err[1:]
        raise subprocess.CalledProcessError(pipe.returncode, ['countAntom',cnfFile], stderr=err)
    return out[solution_index : out.find('\n',solution_index)]

def solver_d4( cnfFile ):
    out = subprocess.check_output( ['d4',cnfFile] ,encoding='utf-8')
    return out[out.find('\ns')+3:-1] #read from console output

def solver_sharpSAT( cnfFile ):
    out = subprocess.check_output( ['sharpSAT','-noIBCP',cnfFile], encoding='utf-8' )
    solution_index = out.find('# solutions \n')+13
    return out[solution_index : out.find('\n',solution_index)]

def solver_dsharp( cnfFile ):
    out = subprocess.check_output( ['dsharp','-noIBCP',cnfFile], encoding='utf-8')
    solution_index = out.find('#SAT (full):')+17
    return out[solution_index : out.find('\n',solution_index)]
    
def solver_miniC2D( cnfFile ):
    out = subprocess.check_output( ['miniC2D','-W','-c',cnfFile], encoding='utf-8')
    solution_index = out.find('Count \t')+7
    return out[solution_index : out.find('.',solution_index)]    #remove digits ".000"

def solver_gpusat( cnfFile ): 
    pipe = subprocess.Popen( ['gpusat','-f',cnfFile], stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE ,encoding='utf-8') #circumvent exit code of 10
    out,err = pipe.communicate()
    solution_index = out.find('"Model Count": ')+15
    if solution_index==14: #not found (=-1+15)
        if err[0] == '\n':
            err = err[1:]
        raise subprocess.CalledProcessError(pipe.returncode, ['gpusat','-f',cnfFile], stderr=err)
    return out[solution_index : out.find('\n',solution_index)]

def solver_dpdb( cnfFile ):
    out = subprocess.check_output( ['python3.8','dpdb.py','-f',str(Path.cwd()/cnfFile),'#sat'],
                stderr=subprocess.STDOUT, cwd=r'/home/patrick/Documents/Model Counter/dpdb/', encoding='utf-8')
    solution_index = out.find('Problem has ')+12
    return out[solution_index : out.find(' models',solution_index)]    

def solver_addmc( cnfFile ):
    out = subprocess.check_output( ['addmc','--wf','1','--cf',cnfFile], encoding='utf-8')
    solution_index = out.find('\ns mc ')+5
    return out[solution_index : out.find('\n',solution_index)]



# list cnf-files in numerical order
def get_files_in_folder(path, suffix):
    files = [f.name   for f in path.iterdir()   if f.suffix == suffix ]
    files = sorted(files, key = numericalSort)
    return files
    
numbers = re.compile(r'(\d+)')
def numericalSort(value): #e.g. value='Test13_from_210' -> parts=['Test', 13 , '-from_', 210]
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts



# -- functions to count homomorphisms --

def get_write_func(phiVersion):
    
    if phiVersion[0] == '-':
        phiVersionTxt = phiVersion[1:]
    else:
        phiVersionTxt = phiVersion
        
    if not phiVersionTxt.isnumeric(): # possibly folder
        return None, phiVersionTxt
        
    phiVersion = int(phiVersion)
    0
    if abs(phiVersion) == 1:
        writeFunc = createPhi.createPhi
        
    elif abs(phiVersion) == 10:
        writeFunc = createPhi.createPhi1_log
        phiVersionTxt = '1_log'
        
    elif abs(phiVersion) == 11:
        writeFunc = createPhi.createPhi1_log_filled
        phiVersionTxt = '1_log_f'
        
    elif abs(phiVersion) == 2:
        writeFunc = createPhi.createPhi2_Tseytin
        
    elif abs(phiVersion) == 3:
        writeFunc = createPhi.createPhi3
        
    elif abs(phiVersion) == 30:
        writeFunc = createPhi.createPhi3_log
        phiVersionTxt = '3_log'
    
    elif abs(phiVersion) == 31:
        writeFunc = createPhi.createPhi3_log_Tseytin
        phiVersionTxt = '3_log_T'
    
    elif abs(phiVersion) == 100:
        writeFunc = createPhi_approx.createPhi_approx
        phiVersionTxt = '1_approx'
        
    elif abs(phiVersion) == 300:
        writeFunc = createPhi_approx.createPhi3_approx
        phiVersionTxt = '3_approx'
        
    else:
        writeFunc = None
        
    return writeFunc, phiVersionTxt
        
    

def get_modelCounter_function(modelCounter):
    
    if modelCounter == 'countAntom':
        mc_Func = solver_countAntom
    
    elif modelCounter == 'd4':
        mc_Func = solver_d4
        
    elif modelCounter == 'sharpSAT':
        mc_Func = solver_sharpSAT
    
    elif modelCounter == 'dsharp':
        mc_Func = solver_dsharp
        
    elif modelCounter == 'miniC2D':
        mc_Func = solver_miniC2D
        
    elif modelCounter == 'gpusat':
        mc_Func = solver_gpusat
        
    elif modelCounter == 'dpdb':
        mc_Func = solver_dpdb
     
    elif modelCounter == 'addmc':
        mc_Func = solver_addmc
    
    else:
        mc_Func = None
    
    return mc_Func
    


def count_with_algorithm(GParser,HParser,name):  
    countFile = 'Runtime/'+ name +'_counts.txt'
    
    ctime = 0    
    i = 1
    with open(countFile,'w') as cF:
    
        for H in HParser.read_graph():
            j = 1
            for G in GParser.read_graph():
                start = time.time()
                h = hom.count_from_bounded_treewidth(H, G)
                end = time.time()
                line = '{i:d} {j:d} : {sol:d} t={t:.3f}s\n'.format(
                    i=i, j=j, sol=h, t=end-start)
                cF.write(line)
                ctime += end-start
                j += 1
            i += 1

    #endWith (closes file)
    print('Counting Models: '+ str(ctime) +'s')


# --- main function ---

if __name__ == '__main__':
    
    #name = 'Peking'
    #HParser = filtered_Parser('Peking_1', 'GraphParser', 0, 2)
    #GParser = filtered_Parser('Peking_1', 'GraphParser', 0)
    
    #name = 'smallPek'
    #HParser = filtered_Parser('Peking_1', 'GraphParser', 20, 2)
    #GParser = filtered_Parser('Peking_1', 'GraphParser', 20)
                              
    name = 'RedTreePek'
    HParser = filtered_Parser('REDDIT-BINARY', 'GraphParser', 20, 1) #dataset, folder, [size], [width]
    GParser = filtered_Parser('Peking_1', 'GraphParser', 20)
    
    phiVersion = 0; #value for not a number
    if len(sys.argv)>1:
        try:
            phiVersion = int(sys.argv[1])
        except ValueError:
            pass
    
    
    # --- run using model counters on default formulas  ---
    if len(sys.argv)>=3:  #default: argv = [ 'main.py', modelCounter, phiVersion, [skippable] ]
        
        mc = sys.argv[1]
        
        writeFunc,phiVersionTxt = get_write_func(sys.argv[2])
        if not writeFunc == None:
            # -- create CNF files --
            phiVersion = int(sys.argv[2])
            cnfFolder = 'OutputCNF/{name:s}_phi{phi:s}'.format(name=name,phi = phiVersionTxt)
            cnfFiles = cnfFolder +'/'+ name +'{}_{}.cnf'
        
            Path(cnfFolder).mkdir(parents=True, exist_ok=True) #create cnf-folder if it doesn't exist
    
            if phiVersion > 0:
                create_CNF_files(writeFunc,GParser,HParser,cnfFiles)
            
            countFile = 'Runtime/{name:s}_phi{phi:s}_{mc:s}_counts.txt'.format(name=name , phi=phiVersionTxt, mc=mc)
        
        else:
            # --read in folder --
            if not Path(sys.argv[2]).is_dir:
                print('Unknown phi version / folder')
                sys.exit(0)
            
            cnfFolder = sys.argv[2]        
            if cnfFolder[-1] == '/':
                cnfFolder = cnfFolder[:-1]
            folder = cnfFolder.split('/')[-1] # top-level folder in the path
            
            countFile = 'Runtime/{folder:s}_{mc:s}_counts.txt'.format(folder=folder , mc=mc)
            

        mc_Func = get_modelCounter_function(mc)
        if mc_Func == None:
            print('Unknown Model Counter')
            sys.exit(0)

            
        # -- run model counter --
        print('CNF-Folder: ' + cnfFolder)
        processes = subprocess.check_output(['ps','-A'])
        if( bytes('mem_warn.sh','utf8') not in processes ):
            subprocess.call('gnome-terminal --title "Memory Observer" -- "mem_warn.sh"', shell=True)
        
        if len(sys.argv)>3 and (sys.argv[3]=='1' or sys.argv[3]=='True'): #skippable option
            solve_all_cnfs_skippable( mc_Func ,cnfFolder, countFile )
        else:
            solve_all_cnfs( mc_Func ,cnfFolder, countFile )
        

    
    
    
    # --- runs the algorithm from masterthesis ---
    elif len(sys.argv)==2 and sys.argv[1]=='algo': # tree-width<=2 [masterthesis 6.2, p.62]
        
        count_with_algorithm(GParser,HParser,name)
    
        
        
    else:
        print('Unknown arguments:')
        
#end main function



