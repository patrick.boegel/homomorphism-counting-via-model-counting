import networkx as nx
import createDNNF
from GraphParser.datasets import Parser
import homomorphism as hom

#import sys            # to read in arguments
import subprocess     # to execute model counters
import time           # to measure time
from pathlib import Path  #to create output directories



def solver_python( dnnfFile,assign=[] ):
    with open(dnnfFile,'r') as f:
        vals = []
        for line in f:
            if len(line)==0:
                pass
            elif line[0] == 'A':
                refs = line[2:].split(' ')
                val = 1
                for ref in refs[1:]: #skip n 
                    val *= vals[int(ref)]
                vals += [val]
            elif line[0] == 'O':
                refs = line[2:].split(' ')
                val = 0
                for ref in refs[2:]: #skip n,i
                    val += vals[int(ref)]
                vals += [val]
            elif line[0] == 'L':
                if -int(line[2:-1]) in assign:
                    vals += [0]
                else:
                    vals += [1]
        return str(vals[-1])


if __name__ == '__main__':
    
    name = 'caterpillar_vector'
    
    maxLen = 30
        
    H = nx.Graph()
    cur = 1
    last = 0
    H.add_node(0, i=0)
    for i in range(1,maxLen+1):
        H.add_node(cur, i=i-1)
        H.add_edge(last,cur)
        for j in range(1,i):
            H.add_node(cur+j, i=i-1)
            H.add_edge(cur,cur+j)
        last = cur
        cur += i+1

    graphs = Parser('AIDS', 'GraphParser').read_graph()
    for i in range(11):
        G = next(graphs)
    
    assert(-1 not in G.nodes)
    G.add_node(-1)
    for v in G.nodes:
        G.add_edge(-1,v)
    
    
    dnnfFile = 'OutputCNF/'+ name +'.nnf'
    
    start = time.time()    
    createDNNF.createPhi_dyn_full(H,G, dnnfFile );
    end = time.time()
    print( ('Creating DNNF-files: '+ str(end-start) +'s'))
    
    
    n = len(G.nodes)
    
    cmd = 'load '+dnnfFile +'\n'
    assign = [];
    for i in range(maxLen):
        cmd += 'mc'
        assign +=[[]]
        for vi,v in enumerate(H.nodes):
            if H.nodes[v]['i'] > i:
                cmd += ' ' + str(vi*n+n)
                assign[-1] += [(vi*n+n)]
            else:
                cmd += ' -'+ str(vi*n+n)
                assign[-1] += [-(vi*n+n)]
        cmd += '\n'
    
    
    '''
    start = time.time()
    pipe = subprocess.Popen( ['query-dnnf'], stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE, stderr=subprocess.PIPE ,encoding='utf-8')
    out,err = pipe.communicate(input=(cmd))
    end = time.time()
    print( ('Counting Models: '+ str(end-start) +'s'))
    
    with open('Runtime/'+ name +'_counts.txt', 'w') as f:
        i = 1
        for line in out.split('\n')[1:]:
            if line[0]=='>' and len(line)>2:
                f.write(str(i) +' 1 : ' + line[2:] + '\n');
                i += 1
            else:
                print(line)
               
    end = time.time()
    print( ('Counting Models: '+ str(end-start) +'s'))
    ''' 
    
    ctime = 0;
    with open('Runtime/'+ name +'_py_counts.txt', 'w') as f:
        for i in range(maxLen):
            start = time.time()
            homCount = solver_python(dnnfFile,assign[i])
            end = time.time()
            line = '{i:d} 1 : {sol:s} t={t:.2f}s'.format(
                        i=int(i),  sol=homCount, t=end-start)
            ctime += end-start
        
    print( ('Counting Models: '+ str(ctime) +'s'))
            






          
